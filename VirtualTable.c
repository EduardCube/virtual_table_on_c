#include <stdio.h>

// Base begin

struct Base
{
//protected:
	void *vptr;
	int functionNumbers;
//private:	
	void **vrTable;
//public:
	int x;
	int y;
};

typedef struct Base Base;

void printAdding(Base* base) // virtual
{
	int sum = base->x + base->y;
	printf("x + y = %d \n", sum);
}

void printSubtracting(Base* base) // virtual
{
	int sub = base->x - base->y;
	printf("x - y = %d \n", sub);
}

Base* BaseConstructor()
{
	Base* base = malloc(sizeof(Base));
	base->x = 8;
	base->y = 5;
	base->functionNumbers = 2; // number of virtual function
	int numOfFunctions = base->functionNumbers;
	base->vrTable = malloc(sizeof(void *) * numOfFunctions);

	// create virtual table of functions for Base class (pointer to function)
	base->vrTable[0] = &printAdding;
	printf("Address of base->vrTable[0]: %p\n", base->vrTable[0]);
	printf("Address of &base->vrTable[0]: %p\n", &base->vrTable[0]);
	printf("Address of printAdding: %p\n\n", &printAdding);

	base->vrTable[1] = &printSubtracting;	
	printf("Address of base->vrTable[1]: %p\n", base->vrTable[1]);
	printf("Address of &base->vrTable[1]: %p\n", &base->vrTable[1]);
	printf("Address of printSubtracting: %p\n\n", &printSubtracting);

	base->vptr = &base->vrTable[0];
	printf("Address of base->vptr: %p\n", base->vptr);
	printf("Address of base->vrTable: %p\n\n", &base->vrTable);
	return base;
}

void BaseDestructor(Base* base)
{
	free(base->vrTable);
	free(base);
}
// Base end



// Derived begin
struct Derived
{
//private:
	Base base;
	void **vrTable;
//public:
	int z;
};

typedef struct Derived Derived;

void printAddingDerived(Derived* derived) // override
{
	int sum = derived->base.x + derived->base.y + derived->z;
	printf("x + y + z = %d \n", sum);
}

void printSubtractingDerived(Derived* derived) // override
{
	int sub = derived->base.x - derived->base.y - derived->z;
	printf("x - y - z = %d \n", sub);
}

Derived* DerivedConstructor()
{
	printf("--------------------------------\n\n");
	Base* b = BaseConstructor();
	Derived* direved = malloc(sizeof(Derived));
	direved->base = *b;
	BaseDestructor(b); // we initialized Base and copied values, so we can delete it now because we have copy

	direved->z = 12;
	int numOfFunctions = direved->base.functionNumbers;
	direved->vrTable = malloc(sizeof(void *) * numOfFunctions);

	// create virtual table of functions for Direved class (pointer to function)
	direved->vrTable[0] = &printAddingDerived;
	printf("Address of direved->vrTable[0]: %p\n", direved->vrTable[0]);
	printf("Address of &direved->vrTable[0]: %p\n", &direved->vrTable[0]);
	printf("Address of printAddingDerived: %p\n\n", &printAddingDerived);

	direved->vrTable[1] = &printSubtractingDerived;
	printf("Address of direved->vrTable[1]: %p\n", direved->vrTable[1]);
	printf("Address of &direved->vrTable[1]: %p\n", &direved->vrTable[1]);
	printf("Address of printSubtractingDerived: %p\n\n", &printSubtractingDerived);

	direved->base.vptr = &direved->vrTable[0];
	printf("Address of direved->base->vptr: %p\n", direved->base.vptr);
	printf("Address of direved->base->vrTable: %p\n\n", &direved->vrTable);
	return direved;
}

void DerivedDestructor(Derived* direved)
{
	free(direved->vrTable);
	free(direved);
}

 //Derived end

void printAddingBase(Base* base)
{
	void *printAddingPtr = *((int*)base->vptr + 0); // get adress of first function from virtual table
	printf("Address of printAddingPtr: %p\n", printAddingPtr);

	void(*fptr)(Base*) = (void(*)(Base*))printAddingPtr;
	fptr(base); // call virtual function, will call Base or Direved function depending of what was passed
}

void printSubtractingBase(Base* base)
{
	void *printSubtractingPtr = *((int*)base->vptr + 1); // get adress of second function from virtual table
	printf("Address of printSubtractingPtr: %p\n", printSubtractingPtr);

	void(*fptr)(Base*) = (void(*)(Base*))printSubtractingPtr;
	fptr(base);
}

void TestPolymorphism (Base* base)
{
	printAddingBase(base);
	printSubtractingBase(base);
}

int main()
{
	Base* base = BaseConstructor();
	Derived* derived = DerivedConstructor();

	TestPolymorphism(base);
	TestPolymorphism(derived);

	BaseDestructor(base);
	DerivedDestructor(derived);
}


